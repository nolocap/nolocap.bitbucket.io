"use strict";

//arrays
let noloThreshold = document.getElementById("nolothresh").value;
let cname = [],cprice = [],ckey = [],cdur = [],cprof = [],sprice = [],skey = [],sdur = [],prikey = [],sprikey = [],nolo = [],rname = [],isbn = [], sisbn = [], onbs = [], nts = [], booktitle = [], sbooktitle = [];
let merge = [["Subject", "Number", "Section", "Price", "NoLo - $" + noloThreshold, "Professor First Name", "Professor Last Name"]];
let rrmerge = [["CRN", "Subject", "Number", "Section", "Course Name", "Price", "NoLo - $" + noloThreshold, "Professor First Name", "Professor Last Name", "Professor Email"]];
let selectedFileRR, selectedFileCL, selectedFileONB, selectedFileNT;
const fileSuccess = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 1500
});
let status = {
  bk: false,
  rr: false,
  onb: false,
  nt: false
};

//nolo Threshold
document.getElementById("nolothresh").addEventListener("change", function() {
  noloThreshold = document.getElementById("nolothresh").value;
  fileSuccess.fire({
    icon: 'success',
    title: 'Threshold Updated'
  });
});
//roomreport parse
document.getElementById("roomreport").addEventListener("change", function() {
  selectedFileRR = event.target.files[0];
  status.rr = true;
  fileSuccess.fire({
    icon: 'success',
    title: 'File Uploaded'
  });
});
//courselist parse
document.getElementById("courselist").addEventListener("change", function() {
  selectedFileCL = event.target.files[0];
  status.bk = true;
  fileSuccess.fire({
    icon: 'success',
    title: 'File Uploaded'
  });
});
//Reset the variables
document.getElementById("resetNL").addEventListener("click", resetForm());
//run parse functions
document.getElementById("findNL").addEventListener("click", function() {
  cname = [],cprice = [],ckey = [],cdur = [],cprof = [],sprice = [],skey = [],sdur = [],prikey = [],sprikey = [],nolo = [],rname = [],isbn = [], sisbn = [], onbs = [], nts = [], booktitle = [], sbooktitle = [];
  merge = [["Subject", "Number", "Section", "Price", "NoLo - " + noloThreshold, "Professor First Name", "Professor Last Name"]];
  rrmerge = [["CRN", "Subject", "Number", "Section", "Course Name", "Price", "NoLo - " + noloThreshold, "Professor First Name", "Professor Last Name", "Professor Email"]];
  if(status.bk === true) {
    if(status.rr === true) {
      //parse bk
      $('#courselist').parse({
        config: {
          delimiter: "auto",
          quotes: false,
          quoteChar: '"',
          escapeChar: '"',
          delimiter: ",",
          complete: CLParse,
        },
        before: function(file, inputElem) {
          console.log("parsing");
        },
        error: function(err, file) {
          console.log("ERROR", err, file);
        },
        complete: function() {
          console.log("CL done");
        }
      });
      //parse rr
      $('#roomreport').parse({
        config: {
          delimiter: "auto",
          quotes: false,
          quoteChar: '"',
          escapeChar: '"',
          delimiter: ",",
          complete: RRParse,
        },
        before: function(file, inputElem) {
          console.log("parsing");
        },
        error: function(err, file) {
          console.log("ERROR", err, file);
        },
        complete: function() {
          console.log("RR done");
        }
      });
      //table or download
      downloadOrTable();
    } else {
      //ask if okay with less data
      Swal.fire({
        icon: 'warning',
        title: 'No Room Report Spreadsheet Detected',
        text: 'This program works better with both the Bookstore Spreadsheet, and Room Report Spreadsheet. Do you still want to continue? (The output will have less data)',
        backdrop: true,
        allowOutsideClick: false,
        showCancelButton: false,
        showConfirmButton: false,
        footer: '<button class="btn-nccO" style="margin-right: 5px;" type="button" id="confirmRR">Confirm</button><button class="btn-nccO" type="button" id="cancelRR">Cancel</button>'
      });
      $('#confirmRR').on("click", function() {
        $('#courselist').parse({
          config: {
            delimiter: "auto",
            quotes: false,
            quoteChar: '"',
            escapeChar: '"',
            delimiter: ",",
            complete: CLParse,
          },
          before: function(file, inputElem) {
            console.log("parsing");
          },
          error: function(err, file) {
            console.log("ERROR", err, file);
          },
          complete: function() {
            console.log("done");
          }
        });
        downloadOrTable();
      });
      $('#cancelRR').on("click", function() {
        Swal.close();
      });
    }
  } else {
    //error missing bk
    Swal.fire({
      icon: 'error',
      title: 'Missing Bookstore Spreadsheet',
      text: 'This program requires a spreadsheet from the Bookstore to work. Be sure to select the correct Bookstore file.',
      backdrop: true,
      allowOutsideClick: false
    });
  }
});

// FUNCTIONS
//Courselist Parse Function
function CLParse(results) {
  let data = results.data;
  console.log(data);
  if (data.length > 1 && data[4].length > 2) {
    if (data[1][1] === "COURSE HISTORY REPORT" && data[8][0] !== "") {
      for (let i = 8; i < data.length; i++) {
        //replace all sections with just an A and put it in a array CNAME
        if (data[i][0] !== "") {
          let title = data[i][0].replace('1(All Sections)', 'A').replace('1(all sections)', 'A').replace('(All Sections)', 'A').replace('All sections', 'A').replace('all sections', 'A').replace('(all sections)', 'A').replace('A(all sections)', 'A').trim().split(" ");
          switch (title[2]) {
            case 'A(A)':
              title[2] = 'A';
              break;
            case 'AA)':
              title[2] = 'A';
              break;
            case 'DS(A)':
              title[2] = 'DS';
              break;
          }
          cname.push(title);
          let prof = data[i + 1][5].split(",");
          [prof[0], prof[1]] = [prof[1], prof[0]];
          cprof.push(prof);
        } else {
          if (data[i][2] === undefined) {

          } else if (data[i][2].includes("REQ") || data[i][2].includes("REC") || data[i][2].includes("SUG") || data[i][2].includes("CHC") || data[i][2].includes("OER") || data[i][2].startsWith("BR")) {
            let key = data[i][2];
            ckey.push({
              key: key
            });
            let dur = data[i][10].trim();
            cdur.push({
              duration: dur
            });
            let code = data[i][11];
            isbn.push({
              isbn: code
            })
            let btitle = data[i][5];
            let regex = /\b(THE|A|OF|IS|AN|AND)\b/gi;
            let res = btitle.replace(regex, '');
            regex = /\W/gi;
            let ress = res.replace(regex, '');
            let auth = data[i][4];
            let resss = ress.replace(auth, '');
            booktitle.push({
              title: resss
            })
          } else {
            let key = "";
            ckey.push({
              key: key
            });
            let dur = "";
            cdur.push({
              duration: dur
            });
            let code = "--";
            isbn.push({
              isbn: code
            })
            let btitle = "";
            booktitle.push({
              title: btitle
            })
          }
          //turns string number into a number type, and puts it into a JSON array CPRICE
          if (data[i][13] === undefined) {

          } else if (data[i][13].startsWith("$")) {
            let price = parseFloat(data[i][13].replace('$', '').trim());
            cprice.push({
              price: price
            });
          } else {
            let price = data[i][13];
            cprice.push({
              price: price
            });
          }
        }
      }

      //f is the index of arrays as multiple arrays are arrays in arrays
      let f = 0;
      //create empty arrays inside an array
      //reset merge incase submitting twice
      merge.length = 0;
      merge = [
        ["Subject", "Number", "Section", "Price", "NoLo - $" + noloThreshold, "Professor First Name", "Professor Last Name"]
      ];
      for (let i = 0; i < cname.length; i++) {
        sprice.push([]);
        skey.push([]);
        sdur.push([]);
        merge.push([]);
        sisbn.push([]);
        sbooktitle.push([]);
      }

      f = -1;
      for (let i = 0; i < cprice.length; i++) {
        if (cprice[i].price === "") {
          f++;
        } else {
          sprice[f].push(cprice[i].price);
        }
      }
      f = -1;
      for (let i = 0; i < ckey.length; i++) {
        if (ckey[i].key === "") {
          f++;
        } else {
          skey[f].push(ckey[i].key);
        }
      }
      f = -1;
      for (let i = 0; i < cdur.length; i++) {
        if (cdur[i].duration === "") {
          f++;
        } else {
          sdur[f].push(cdur[i].duration);
        }
      }
      f = -1;
      for (let i = 0; i < booktitle.length; i++) {
        if (booktitle[i].title === "") {
          f++;
        } else {
          sbooktitle[f].push(booktitle[i].title);
        }
      }
      f = -1;
      for (let i = 0; i < isbn.length; i++) {
        if (isbn[i].isbn === "--") {
          f++;
        } else {
          sisbn[f].push(Number(isbn[i].isbn));
        }
      }

      //match price with ckey
      for (let i = 0; i < sprice.length; i++) {
        prikey.push([]);
      }
      f = 0
      for (let i = 0; i < sprice.length; i++) {
        prikey[f] = [skey[i], sdur[i], sprice[i], sisbn[i], sbooktitle[i]];
        f++;
      }
    //


    noloThreshold = document.getElementById("nolothresh").value;
    for(let i = 0; i < prikey.length; i++) {
      let books = {};
      for(let j = 0; j < prikey[i][0].length; j++) {
        if((prikey[i][1][j] === "PURCHASE" || prikey[i][1][j] === "N/A") && prikey[i][3][j] !== 0) {
            if (typeof books[prikey[i][4][j]] === "undefined") {
                books[prikey[i][4][j]] = 0;
            }
            books[prikey[i][4][j]] = Math.max(parseFloat(prikey[i][2][j]),books[prikey[i][4][j]]);
        }
      }
      let totalprice = 0.0;
      for (let b in books) {
          totalprice += books[b];
      }
      if(totalprice === 0) {
        let lowprice = Math.min.apply(Math, prikey[i][2]);
        totalprice = lowprice;
      }
      if(totalprice <= noloThreshold) {
        nolo.push("Yes");
      } else {
        nolo.push("No");
      }
      sprikey.push(totalprice);
    }



    //

      let formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
      });

      for(let i = 0; i < sprikey.length; i++) {
       sprikey[i] = formatter.format(sprikey[i]);
      }


      f = 1;
      for (let i = 0; i < cname.length; i++) {
        merge[f].push(cname[i][0]);
        merge[f].push(cname[i][1]);
        merge[f].push(cname[i][2]);
        merge[f].push(sprikey[i]);
        merge[f].push(nolo[i]);
        merge[f].push(cprof[i][0]);
        merge[f].push(cprof[i][1]);
        f++;
      }

      let prevc;
      for (let i = 0; i < merge.length; i++) {
        if (prevc === (merge[i][0] + merge[i][1] + merge[i][2])) {
          merge.splice(i, 1);
        }
        prevc = (merge[i][0] + merge[i][1] + merge[i][2])
      }

    } else {
      return Swal.fire({
        icon: 'error',
        title: 'Wrong File Submitted',
        text: 'The uploaded spreadsheet does not match the required format. Double-check you submitted the Course List Spreadsheet.',
        backdrop: true,
        allowOutsideClick: false
      });
    }
  } else {
    return Swal.fire({
      icon: 'error',
      title: 'Wrong File Submitted',
      text: 'The uploaded spreadsheet does not match the required format. Double-check you submitted the Course List Spreadsheet.',
      backdrop: true,
      allowOutsideClick: false
    });
  }
}
//RoomReport Parse Function
function RRParse(results) {
  let data = results.data;
    if (data[0][0] === "BLDG" && data[0][1] === "RM" && data[0][2] === "RM_CPTY" && data[0][3] === "CP" && data[0][4] === "CRN" && data[1][0] !== "") {
      for(let i = 0; i < data.length; i++) {
        if(data[i][4] === undefined) {

        } else if(data[i][30] === undefined) {
          rname.push({
            crn: data[i][4],
            course: data[i][5] + data[i][6] + data[i][7],
            title: data[i][8],
            email: "N/A"
          });
        } else {
          rname.push({
            crn: data[i][4],
            course: data[i][5] + data[i][6] + data[i][7],
            title: data[i][8],
            email: data[i][30]
          });
        }
      }

      //remove duplicates from rname
      let prevc;
      for (let i = 0; i < rname.length; i++) {
        prevc = rname[i].crn;
        if (prevc === rname[i].crn) {
          rname.splice(i, 1);
        }
      }
      //Match Coure Title with RNAME
      for (let i = 1; i < merge.length; i++) {
        for (let j = 0; j < rname.length; j++) {
          if ((merge[i][0] + merge[i][1] + merge[i][2]) === rname[j].course) {
            rrmerge.push([rname[j].crn, merge[i][0], merge[i][1], merge[i][2], rname[j].title, merge[i][3], merge[i][4], merge[i][5], merge[i][6], rname[j].email]);
          }
        }
      }
      merge = rrmerge;
    } else {
      return Swal.fire({
        icon: 'error',
        title: 'Wrong File Submitted',
        text: 'The uploaded spreadsheet does not match the required format. Double-check you submitted the Room Report Spreadsheet.',
        backdrop: true,
        allowOutsideClick: false
      });
    }
}
//Download a CSV or Show a Table
function downloadOrTable() {
  Swal.fire({
    icon: 'question',
    title: 'Would you like to view the results in browser, or download a spreadsheet',
    backdrop: true,
    allowOutsideClick: false,
    showConfirmButton: false,
    showCloseButton: true,
    footer: '<a id="browserview"><button style="margin-right: 5px;" class="btn-nccO" type="button">View in Browser</button></a><a id="dload"><button class="btn-nccO" type="button">Download</button></a>'
  });

  $('#browserview').on("click", function() {
    createTable();
  });
  $('#dload').on("click", function() {
    downloadButton();
  });
}
//Creates a Modal with Download Button
function downloadButton() {
  let csvContent = "data:text/csv;charset=utf-8," + merge.map(e => e.join(",")).join("\n");
  let encodedUri = encodeURI(csvContent);
  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = today.getFullYear();
  today = mm + '-' + dd + '-' + yyyy;
  let fileTitle = today + " NoLoCourses.csv";
  Swal.fire({
    icon: 'success',
    title: 'Your spreadsheet is ready for download!',
    backdrop: true,
    allowOutsideClick: false,
    showConfirmButton: false,
    showCloseButton: true,
    footer: '<a id="downloader"><button class="btn-nccO" type="button">Download</button></a>'
  });
  $('#downloader').attr('href', encodedUri);
  $('#downloader').attr('download', fileTitle);
}
//Hides form and creates the tables
function createTable() {
  Swal.close();
  if(merge[0][0] === "CRN") {
    let dtmerge = merge;
    dtmerge.shift();
    $('#centerRow').addClass('hide');
    if(dtmerge.length < 5) {

    } else {
      $('.footer').removeClass('footer').addClass('footerT');
    }
    $('#datatable').DataTable( {
          data: dtmerge,
          "lengthMenu": [[-1], ["All"]],
          "paging":   false,
          "scrollX": false,
          "info": false,
          columns: [
              { title: "CRN" },
              { title: "Subject" },
              { title: "Number" },
              { title: "Section" },
              { title: "Title" },
              { title: "Price" },
              { title: "NoLo" },
              { title: "Professor First Name" },
              { title: "Professor Last Name" },
              { title: "Professor Email" }
          ]
      } );
  } else {
    let dtmerge = merge;
    dtmerge.shift();
    $('#centerRow').addClass('hide');
    if(dtmerge.length < 5) {

    } else {
      $('.footer').removeClass('footer').addClass('footerT');
    }
    $('#datatable').DataTable( {
          data: dtmerge,
          "lengthMenu": [[-1], ["All"]],
          "paging":   false,
          "scrollX": false,
          "info": false,
          columns: [
              { title: "Subject" },
              { title: "Number" },
              { title: "Section" },
              { title: "Price" },
              { title: "NoLo" },
              { title: "Professor First Name" },
              { title: "Professor Last Name" }
          ]
      } );
  }
  $("#newSearchPlace").html($(".dataTables_filter"));
}
//Reset Arrays and Selected Files
function resetForm() {
  selectedFileCL = undefined;
  selectedFileRR = undefined;
  selectedFileNT = undefined;
  selectedFileONB = undefined;
  status = {
    bk: false,
    rr: false,
    onb: false,
    nt: false
  };
  cname = [],cprice = [],ckey = [],cdur = [],cprof = [],sprice = [],skey = [],sdur = [],prikey = [],sprikey = [],nolo = [],rname = [],isbn = [], sisbn = [], onbs = [], nts = [], booktitle = [], sbooktitle = [];
  merge = [["Subject", "Number", "Section", "Price", "NoLo - " + noloThreshold, "Professor First Name", "Professor Last Name"]];
  rrmerge = [["CRN", "Subject", "Number", "Section", "Course Name", "Price", "NoLo - " + noloThreshold, "Professor First Name", "Professor Last Name", "Professor Email"]];
}

//disabled functions
/*
//oer no book parse
document.getElementById("oernobook").addEventListener("change", function() {
  selectedFileONB = event.target.files[0];
  status.onb = true;
  fileSuccess.fire({
    icon: 'success',
    title: 'File Uploaded'
  });
});
//no title parse
document.getElementById("notitle").addEventListener("change", function() {
  selectedFileNT = event.target.files[0];
  status.nt = true;
  fileSuccess.fire({
    icon: 'success',
    title: 'File Uploaded'
  });
});
//OER No Book Parse Function
function ONBParse(results) {
  let data = results.data;
  for(let i = 1; i < data.length - 1; i++) {
    if(data[i] !== "") {
      let prof = data[i][5].split(',');
      onbs.push([data[i][3], data[i][0], data[i][1], data[i][2], data[i][4], data[i][6], "Yes", prof[1].trim(), prof[0].trim(), "Email Unavailable"])
    } else {

    }
  }
  for(let i = 0; i < onbs.length; i++) {
    merge.push(onbs[i]);
  }
}
//No Title Parse Function
function NTParse(results) {
  let data = results.data;
  for(let i = 1; i < data.length - 1; i++) {
    if(data[i] !== "") {
      let prof = data[i][5].split(',');
      nts.push([data[i][3], data[i][0], data[i][1], data[i][2], data[i][4], data[i][6], "Yes", prof[1].trim(), prof[0].trim(), "Email Unavailable"])
    } else {

    }
  }
  for(let i = 0; i < nts.length; i++) {
    merge.push(nts[i]);
  }
}
*/
