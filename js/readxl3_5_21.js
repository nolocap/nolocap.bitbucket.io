"use strict";
let noloThreshold = document.getElementById("nolothresh").value;
document.getElementById("nolothresh").addEventListener("change", function() {
  noloThreshold = document.getElementById("nolothresh").value;
})

//roomreport parse
let selectedFileRR;
document.getElementById("roomreport").addEventListener("change", function() {
  selectedFileRR = event.target.files[0];
});

//courselist parse
let selectedFileCL;
document.getElementById("courselist").addEventListener("change", function() {
  selectedFileCL = event.target.files[0];
});

document.getElementById("findNL").addEventListener("click", function() {
  if(!selectedFileRR) {
    Swal.fire({
      icon: 'warning',
      title: 'No Room Report Spreadsheet Detected',
      text: 'This program works better with both the Bookstore Spreadsheet, and Room Report Spreadsheet. Do you still want to continue? (The output will have less data)',
      backdrop: true,
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
      footer: '<button class="btn-nccO" style="margin-right: 5px;" type="button" id="confirmRR">Confirm</button><button class="btn-nccO" type="button" id="cancelRR">Cancel</button>'
    });
    $('#cancelRR').on("click", function() {
      Swal.close();
    });
    $('#confirmRR').on("click", function() {
      if(selectedFileCL) {
        $('#courselist').parse({
          config: {
            delimiter: "auto",
            quotes: false,
    	      quoteChar: '"',
    	      escapeChar: '"',
    	      delimiter: ",",
            complete: showResults,
          },
          before: function(file, inputElem) {
            console.log("parsing");
          },
          error: function(err, file) {
            console.log("ERROR", err, file);
          },
          complete: function() {
            console.log("done");
          }
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Missing Bookstore Spreadsheet',
          text: 'This program requires a spreadsheet from the Bookstore to work. Be sure to select the correct Bookstore file.',
          backdrop: true,
          allowOutsideClick: false
        });
      }
    });
  }
});

//arrays
const cname = [];
const cprice = [];
const ckey = [];
const cprof = [];
const all = [];
const merge = [["Professor Last Name", "Professor First Name", "Subject", "Number", "Section"]];

//Function after papaparse
function showResults(results) {
  let data = results.data;
  for(let i = 8; i < data.length; i++) {
    //replace all sections with just an A and put it in a JSON array CNAME
    if(data[i][0] !== "") {
      let title = data[i][0].replace('(All Sections)', 'A').replace('All sections', 'A').replace('all sections', 'A').replace('(all sections)', 'A').trim();
      cname.push({title: title});
      let prof = data[i+1][5];
      cprof.push({prof: prof})
    } else {
      if(data[i][2] === undefined) {

      } else if(data[i][2].includes("REQ") || data[i][2].includes("REC") || data[i][2].includes("SUG") || data[i][2].includes("CHC") || data[i][2].includes("OER") || data[i][2].startsWith("BR")) {
        let key = data[i][2];
        ckey.push({key: key});
      } else  {
        let key = "";
        ckey.push({key: key});
      }
      //turns string number into a number type, and puts it into a JSON array CPRICE
      if(data[i][13] === undefined) {

      } else if(data[i][13].startsWith("$")) {
        let price = parseFloat(data[i][13].replace('$', '').trim());
        cprice.push({price: price});
      } else {
        let price = data[i][13];
        cprice.push({price: price});
      }
    }
  }
  let j = 0;
  //puts cname and cprice into one giant array unsorted
  for(let i = 0; i < cprice.length; i++) {
    if(cprice[i].price === "") {
      all.push(cname[j].title);
      j++
    } else {
      all.push(ckey[i].key);
      all.push(cprice[i].price);
    }
  }

  //makes sure that merge is not empty
  let f = 0;
  for(let i = 0; i < cname.length; i++) {
    merge.push([]);
  }

  //merges the all array to create smaller arrays with title and price
  for(let i = 0; i < all.length; i++) {
    if(isNaN(all[i])) {
      if(all[i].startsWith("REQ") || all[i].startsWith("REC") || all[i].startsWith("SUG") || all[i].startsWith("CHC") || all[i].startsWith("OER") || all[i].startsWith("BR")) {
        merge[f].push(all[i]);
      } else {
        f++;
        merge[f].push(all[i].split(" "));
      }
    } else {
      merge[f].push(all[i]);
    }
  }

  //add prof name to array
  f = 1;
  for(let i = 0; i < cprof.length; i++) {
    let prof = cprof[i].prof.split(",");
    merge[f].unshift(prof);
    f++;
  }



  let csvContent = "data:text/csv;charset=utf-8," + merge.map(e => e.join(",")).join("\n");
  let encodedUri = encodeURI(csvContent);
  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = today.getFullYear();
  today = mm + '-' + dd + '-' + yyyy;
  let fileTitle = today + " NoLoCourses.csv";
  Swal.fire({
    icon: 'success',
    title: 'Your spreadsheet is ready for download!',
    backdrop: true,
    allowOutsideClick: false,
    showConfirmButton: false,
    showCloseButton: true,
    footer: '<a id="downloader"><button class="btn-nccO" type="button">Download</button></a>'
  });
  $('#downloader').attr('href', encodedUri);
  $('#downloader').attr('download', fileTitle);
}
