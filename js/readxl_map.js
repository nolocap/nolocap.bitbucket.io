"use strict";
//MAP APPROACH

//Define Variables
let selectedFileRR = undefined;
let selectedFileCL = undefined;
let noloThreshold = document.getElementById("nolothresh").value;

//Set noloThreshold equal to Value entered by user
document.getElementById("nolothresh").addEventListener("change", function() {
  noloThreshold = document.getElementById("nolothresh").value;
});

//Set selectedFileRR equal to the uploaded file
document.getElementById("roomreport").addEventListener("change", function() {
  selectedFileRR = event.target.files[0];
});

//Set selectedFileCL equal to the uploaded file
document.getElementById("courselist").addEventListener("change", function() {
  selectedFileCL = event.target.files[0];
});

//Reset the variables
document.getElementById("resetNL").addEventListener("click", function() {
  selectedFileCL = undefined;
  selectedFileRR = undefined;
})

//On Submit check what has been submitted
document.getElementById("findNL").addEventListener("click", function() {
  /*
  if(!selectedFileRR) {
    Swal.fire({
      icon: 'warning',
      title: 'No Room Report Spreadsheet Detected',
      text: 'This program works better with both the Bookstore Spreadsheet, and Room Report Spreadsheet. Do you still want to continue? (The output will have less data)',
      backdrop: true,
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
      footer: '<button class="btn-nccO" style="margin-right: 5px;" type="button" id="confirmRR">Confirm</button><button class="btn-nccO" type="button" id="cancelRR">Cancel</button>'
    });
    $('#cancelRR').on("click", function() {
      Swal.close();
    });
    $('#confirmRR').on("click", function() {
      if(selectedFileCL) {
        $('#courselist').parse({
          config: {
            delimiter: "auto",
            quotes: false,
    	      quoteChar: '"',
    	      escapeChar: '"',
    	      delimiter: ",",
            complete: showResults,
          },
          before: function(file, inputElem) {
            console.log("parsing");
          },
          error: function(err, file) {
            console.log("ERROR", err, file);
          },
          complete: function() {
            console.log("done");
          }
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Missing Bookstore Spreadsheet',
          text: 'This program requires a spreadsheet from the Bookstore to work. Be sure to select the correct Bookstore file.',
          backdrop: true,
          allowOutsideClick: false
        });
      }
    });
  } else {
    $('#courselist').parse({
      config: {
        delimiter: "auto",
        quotes: false,
        quoteChar: '"',
        escapeChar: '"',
        delimiter: ",",
        complete: showResultsRR,
      },
      before: function(file, inputElem) {
        console.log("parsing");
      },
      error: function(err, file) {
        console.log("ERROR", err, file);
      },
      complete: function() {
        downloadButton();
      }
    });
  }  */

  if(selectedFileCL) {
    $('#courselist').parse({
      config: {
        delimiter: "auto",
        quotes: false,
        quoteChar: '"',
        escapeChar: '"',
        delimiter: ",",
        complete: showResults,
      },
      before: function(file, inputElem) {
        console.log("parsing");
      },
      error: function(err, file) {
        console.log("ERROR", err, file);
      },
      complete: function() {
        console.log("done");
      }
    })
  }

});

let bkstoreMap = new Map();
let rmMap = new Map();
let ctitle, cprof, cprice, tprice, ttprice, cnolo;
let rcrn, rtitle, remail;
let merge =  [["Subject", "Number", "Section", "Price", "NoLo", "Professor First Name", "Professor Last Name"]];

//Function after papaparse
function showResults(results) {
  let data = results.data;
  for(let i = 8; i < data.length; i++) {
    if(data[i][0] !== "") {
      ctitle = data[i][0].replace('1(All Sections)', 'A').replace('1(all sections)', 'A').replace('(All Sections)', 'A').replace('All sections', 'A').replace('all sections', 'A').replace('(all sections)', 'A').replace('A(all sections)', 'A').trim().replace(/\W/g,'');
      cprof = data[i+1][5].split(",");
      [cprof[0], cprof[1]] = [cprof[1], cprof[0]];
      cprice = 0;
    } else {
      if(!/\w{3}-\d\d/.test(data[i][2])) {
        if(data[i][2] === undefined) {

        } else if((data[i][2].includes("REQ")) && (data[i][10].includes("PURCHASE"))) {
          tprice = parseFloat(data[i][13].replace(/[^\d.]/g,''));
          cprice += tprice;
        }
      }
    }
    if(cprice > 0) {
      if(cprice <= noloThreshold) {
        cnolo = "Yes";
      } else {
        cnolo = "No"
      }
    } else {
      cnolo = "No";
    }
    bkstoreMap.set(ctitle, {price: cprice, prof: cprof, nolo: cnolo});
  }
  //console.log(bkstoreMap);
  //bkstoreMap.forEach((values, keys) => {
  //  merge.push([keys, values.price, values.nolo, values.prof[0], values.prof[1]]);
  //});

  let fileReader = new FileReader();
  fileReader.readAsBinaryString(selectedFileRR);
  fileReader.onload = (event)=>{
   let data = event.target.result;
   let workbook = XLSX.read(data,{type:"binary"});
   let rowObject = XLSX.utils.sheet_to_row_object_array(workbook.Sheets["All Courses"]);
   //console.log(rowObject);
   for(let i = 0; i < rowObject.length; i++) {
     if(rowObject[i].CRN === undefined) {

     } else {
       rcrn = rowObject[i].CRN;
       rtitle = rowObject[i].SUBJ + rowObject[i].CRSE + rowObject[i].SECT;
       remail = rowObject[i].EMAIL;
       rmMap.set(rcrn, {title: rtitle, email: remail});
     }
   }
   console.log(rmMap);
   rmMap.forEach((rval, rkey) => {
    //if(bkstoreMap.get(rkey) === undefined) {

    //} else {
        console.log(rkey + " " + bkstoreMap.get(rkey));
    //}
});
//NEXT JUST PUT THE CRNS INTO AN ARRAY :)

}
}


function downloadButton() {
  let csvContent = "data:text/csv;charset=utf-8," + merge.map(e => e.join(",")).join("\n");
  let encodedUri = encodeURI(csvContent);
  let today = new Date();
  let dd = String(today.getDate()).padStart(2, '0');
  let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = today.getFullYear();
  today = mm + '-' + dd + '-' + yyyy;
  let fileTitle = today + " NoLoCourses.csv";
  Swal.fire({
    icon: 'success',
    title: 'Your spreadsheet is ready for download!',
    backdrop: true,
    allowOutsideClick: false,
    showConfirmButton: false,
    showCloseButton: true,
    footer: '<a id="downloader"><button class="btn-nccO" type="button">Download</button></a>'
  });
  $('#downloader').attr('href', encodedUri);
  $('#downloader').attr('download', fileTitle);
}
